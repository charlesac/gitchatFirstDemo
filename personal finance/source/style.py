import tkinter.font as tkFont
import tkinter as tk

def _font(fname="微软雅黑",size=12,bold=tkFont.NORMAL):
    ft=tkFont.Font(family=fname,size=size,weight=bold)
    return ft

def _ft(size=12,bold=False):
    if bold:
        return _font(size=size,bold=tkFont.BOLD)
    else:
        return _font(size=size,bold=tkFont.NORMAL)
        
def h_seperator(parent,height=2):
    tk.Frame(parent,height=height,bg="whitesmoke").pack(fill=tk.X)

def v_seperator(parent,width,bg='WhiteSmoke'):
    frame=tk.Frame(parent,width=width,bg=bg)
    return frame