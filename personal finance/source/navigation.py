import tkinter as tk
import style as st
import view as view

class navi():
    def createLabel(self,parent,flag,text,command):
        self.root=parent
        if flag==1:
            f=tk.Frame(self.root,width=150)
            st.v_seperator(f,width=5,bg="blue").pack(side=tk.LEFT,fill=tk.Y)
            button1=tk.Button(f,borderwidth=0,width="10",text=text,fg="black",font=st._ft(10,False),command=command)
            button1.pack(side=tk.LEFT,anchor=tk.W,padx=35,pady=5)
            f.pack(fill=tk.X)
        else:
            f=tk.Frame(self.root,bg="DarkSlateGray",width=150)
            button1=tk.Button(f,borderwidth=0,width="10",text=text,bg="DarkSlateGray",fg="white",font=st._ft(10,False),command=command)
            button1.pack(side=tk.LEFT,anchor=tk.W,padx=35,pady=5)
            f.pack(fill=tk.X)
        return f
        
    def __init__(self,parent,parent2):
        self.root=parent
        self.conRoot=parent2
        self.content=tk.Frame(self.conRoot,bg="whitesmoke")
        self.content.pack(expand='yes',fill='both')
        self.l1=tk.Label(self.root,text="报表整理",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l1.pack(anchor=tk.W,padx=20,pady=10)
        self.f1=self.createLabel(self.root,1,"每日文件导入",self.func1)
        self.f2=self.createLabel(self.root,0,"功能二",self.func2)
        self.f3=self.createLabel(self.root,0,"功能三",self.func3)
        self.l2=tk.Label(self.root,text="数据分析",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l2.pack(anchor=tk.W,padx=20,pady=10)
        self.f4=self.createLabel(self.root,0,"收支统计",self.func4)
        self.func1()
    
    def func1(self):
        self.f1.destroy()
        self.f2.destroy()
        self.f3.destroy()
        self.f4.destroy()
        self.l1.pack_forget()
        self.l2.pack_forget()
        self.content.destroy()
        self.content=view.view1(self.conRoot)
        self.l1=tk.Label(self.root,text="报表整理",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l1.pack(anchor=tk.W,padx=20,pady=10)
        self.f1=self.createLabel(self.root,1,"每日文件导入",self.func1)
        self.f2=self.createLabel(self.root,0,"功能二",self.func2)
        self.f3=self.createLabel(self.root,0,"功能三",self.func3)
        self.l2=tk.Label(self.root,text="数据分析",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l2.pack(anchor=tk.W,padx=20,pady=10)
        self.f4=self.createLabel(self.root,0,"收支统计",self.func4)
    
    def func2(self):
        self.f1.destroy()
        self.f2.destroy()
        self.f3.destroy()
        self.f4.destroy()
        self.l1.pack_forget()
        self.l2.pack_forget()
        self.content.destroy()
        self.content=view.view2(self.conRoot)
        self.l1=tk.Label(self.root,text="报表整理",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l1.pack(anchor=tk.W,padx=20,pady=10)
        self.f1=self.createLabel(self.root,0,"每日文件导入",self.func1)
        self.f2=self.createLabel(self.root,1,"功能二",self.func2)
        self.f3=self.createLabel(self.root,0,"功能三",self.func3)
        self.l2=tk.Label(self.root,text="数据分析",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l2.pack(anchor=tk.W,padx=20,pady=10)
        self.f4=self.createLabel(self.root,0,"收支统计",self.func4)
    
    def func3(self):
        self.f1.destroy()
        self.f2.destroy()
        self.f3.destroy()
        self.f4.destroy()
        self.l1.pack_forget()
        self.l2.pack_forget()
        self.content.destroy()
        self.content=view.view3(self.conRoot)
        self.l1=tk.Label(self.root,text="报表整理",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l1.pack(anchor=tk.W,padx=20,pady=10)
        self.f1=self.createLabel(self.root,0,"每日文件导入",self.func1)
        self.f2=self.createLabel(self.root,0,"功能二",self.func2)
        self.f3=self.createLabel(self.root,1,"功能三",self.func3)
        self.l2=tk.Label(self.root,text="数据分析",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l2.pack(anchor=tk.W,padx=20,pady=10)
        self.f4=self.createLabel(self.root,0,"收支统计",self.func4)
    
    def func4(self):
        self.f1.destroy()
        self.f2.destroy()
        self.f3.destroy()
        self.f4.destroy()
        self.l1.pack_forget()
        self.l2.pack_forget()
        self.content.destroy()
        self.content=view.view4(self.conRoot)
        self.l1=tk.Label(self.root,text="报表整理",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l1.pack(anchor=tk.W,padx=20,pady=10)
        self.f1=self.createLabel(self.root,0,"每日文件导入",self.func1)
        self.f2=self.createLabel(self.root,0,"功能二",self.func2)
        self.f3=self.createLabel(self.root,0,"功能三",self.func3)
        self.l2=tk.Label(self.root,text="数据分析",fg="white",bg="DarkSlateGray",font=st._ft(12,True))
        self.l2.pack(anchor=tk.W,padx=20,pady=10)
        self.f4=self.createLabel(self.root,1,"收支统计",self.func4)