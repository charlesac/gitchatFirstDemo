# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 10:12:39 2020

@author: 016322684
"""
#导入包
import tkinter as tk
import tkinter.messagebox
import xlrd
import os
from xlutils.copy import copy

#定义导入账单功能函数
def insertreport(path):
    count={}
    with open(path,'r',encoding='utf-8') as f:
        #按行读取文件
        for line in f.readlines():
            line = line.strip('\n')
            year_month = line.split(' ',1)[0][0:6]
        #按年月整理为字典格式
            if count.get(year_month)==None:
                count[year_month]=set()
                count[year_month].add(line)
            else:
                count[year_month].add(line)
    for items in count:
        #通过相对路径查找年度报表文件是否存在，如果不存在则从模板新建，若存在则修改存在的文件
        if os.path.exists('.\\..\\resource\\'+items[0:4]+'.xls'):
            x1 = xlrd.open_workbook('.\\..\\resource\\'+items[0:4]+'.xls')
        else:
            x1 = xlrd.open_workbook('.\\..\\resource\\'+'template.xls')
        sht_read = x1.sheet_by_index(0)
        wb = copy(x1)
        sht = wb.get_sheet(0)
        month = int(items[5:6])
        value = sht_read.col_values((month-1)*3)
        print(value)
        #获取当前填写的行数，在下一行进行填写
        num = len(value)
        while(value[num-1]==""):
            num-=1
        #填入每一项对应内容
        for item in count[items]:
            itemList = item.split(' ')
            sht.write(num,(month-1)*2,itemList[1])
            sht.write(num,(month-1)*2+1,itemList[2])            
            num+=1
        #保存报表
        wb.save('.\\..\\resource\\'+items[0:4]+'.xls')
    tk.messagebox.showinfo('提示','已完成导入')
    return

#定义统计收入支出函数
def static(path):
    x1 = xlrd.open_workbook(path)
    sht_read = x1.sheet_by_index(0)
    wb = copy(x1)
    sht = wb.get_sheet(0)    

    #依次统计12个月的收入支出
    for i in range(1,12):
        income=0
        pay=0
        values = sht_read.col_values(i*2-1)[6:]
        print(values)
        for value in values:
            if(value==""):
                continue
            elif(value[0]=="+"):
                income+=int(value[1:])
            elif(value[0]=="-"):
                pay+=int(value[1:])
        sht.write(3,i*2-1,income)
        sht.write(4,i*2-1,pay)

    #保存报表
    wb.save(path) 
    tk.messagebox.showinfo('提示','已完成统计')
    return