import tkinter as tk
import navigation as ng
import style as st

if __name__=='__main__':
	mainWindow = tk.Tk()
	mainWindow.title("个人财务报表管理")
	mainWindow.geometry('900x600')
	frameTitle = tk.Frame(mainWindow,height=80,bg='LightSkyBlue')
	labelTitle = tk.Label(frameTitle,text='个人财务报表管理',bg="LightSkyBlue",fg="white",height=2,font=st._ft(12,'true'))
	labelTitle.pack(side=tk.LEFT,padx=10)
	frameNav=tk.Frame(mainWindow,width=150,bg="DarkSlateGray")
	frameCon=tk.Frame(mainWindow,width=50,bg="whitesmoke")
	frameTitle.pack(side='top',expand='no',fill='x')
	frameNav.pack(side='left',fill='y')
	frameCon.pack(side='right',expand='yes',fill='both')
	ng.navi(frameNav,frameCon)
	mainWindow.mainloop()