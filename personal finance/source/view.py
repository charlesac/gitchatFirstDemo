import tkinter as tk
import tkinter.filedialog as tf
import service as sv


def view1(parent):
    f=tk.Frame(parent,bg="whitesmoke")
    f.pack(expand='yes',fill='both')
    def selectPath():
        path_=tf.askopenfilename()
        path.set(path_)
    path=tk.StringVar()
    tk.Label(f,text="请选择导入文件").grid(row=0,column=0,padx=5,pady=10)
    tk.Entry(f,textvariable=path).grid(row=0,column=1,padx=5,pady=10)
    tk.Button(f,text="文件选择",command=selectPath).grid(row=0,column=2,padx=5,pady=10)
    tk.Button(f,text="导入报表",command=lambda:sv.insertreport(path.get())).grid(row=0,column=3,padx=5,pady=10)
    return f

def view2(parent):
    f=tk.Frame(parent,bg="whitesmoke")
    f.pack(expand='yes',fill='both')
    return f
    
def view3(parent):
    f=tk.Frame(parent,bg="whitesmoke")
    f.pack(expand='yes',fill='both')
    return f

def view4(parent):
    f=tk.Frame(parent,bg="whitesmoke")
    f.pack(expand='yes',fill='both')
    def selectPath():
        path_=tf.askopenfilename()
        path.set(path_)
    path=tk.StringVar()
    tk.Label(f,text="请选择统计文件").grid(row=0,column=0,padx=5,pady=10)
    tk.Entry(f,textvariable=path).grid(row=0,column=1,padx=5,pady=10)
    tk.Button(f,text="文件选择",command=selectPath).grid(row=0,column=2,padx=5,pady=10)
    tk.Button(f,text="收支统计",command=lambda:sv.static(path.get())).grid(row=0,column=3,padx=5,pady=10)
    return f