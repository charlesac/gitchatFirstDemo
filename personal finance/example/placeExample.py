# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 10:42:08 2020

@author: 016322684
"""

#导入tkinter包
import tkinter as tk
import tkinter.font as tkFont
#创建窗口
window=tk.Tk()
#窗口命名
window.title('place演示')
window.geometry('300x200')
#创建3个标签组件
tk.Label(window,text='组件1',bg='blue',font=tkFont.Font(family="微软雅黑",size=12,weight='bold')).place(relx=0.5,rely=0.6)
tk.Label(window,text='组件2',bg='red',font=tkFont.Font(family="微软雅黑",size=12,weight='bold')).place(x=20,y=30)
tk.Label(window,text='组件3',bg='yellow',font=tkFont.Font(family="微软雅黑",size=12,weight='bold')).place(x=50,y=60)

window.mainloop()