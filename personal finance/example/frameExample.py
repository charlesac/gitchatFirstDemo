# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 21:47:59 2020

@author: charl
"""

import tkinter as tk
#创建窗口
window=tk.Tk()
#窗口命名
window.title('frame演示')
#设置窗口大小
window.geometry('300x200')
#创建容器1
frame1=tk.Frame(window,width=150,height=200,bg='yellow')
#将容器1放置在主窗口左侧
frame1.pack(side='left')
#创建容器2
frame2=tk.Frame(window,width=150,height=200,bg='red')
#将容器2放置在主窗口右侧
frame2.pack(side='right')
#主窗口循环显示
window.mainloop()