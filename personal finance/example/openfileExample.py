# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 10:36:04 2020

@author: 016322684
"""

#打开文件
with open('test.txt','r') as f:
#按行读取文件
    for line in f.readlines():
#打印每一行，去掉\n
        print(line.strip())