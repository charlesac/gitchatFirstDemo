# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 12:28:41 2020

@author: 016322684
"""

# #导入excel读取包
# import xlrd
# #建立excel表连接
# x1 = xlrd.open_workbook("data.et")
# # 获取所有sheet名字
# print('sheet_names:', x1.sheet_names())
# # 获取sheet数量
# print('sheet_number:', x1.nsheets)
# # 获取所有sheet对象    
# print('sheet_object:', x1.sheets())
# # 通过sheet名查找   
# print('By_name:', x1.sheet_by_name("Sheet1"))
# # 通过索引查找
# print('By_index:', x1.sheet_by_index(0))
# #获取第一个sheet页
# sht1 = x1.sheet_by_name("Sheet1")
# #获取行数
# print("row num:", sht1.nrows)
# #获取列数
# print("col num:", sht1.ncols)
# #读取特定单元格值
# sht1.cell_value(1, 0)
# #单元格批量读取,读取第一行所有内容
# print("row num:",sht1.row_values(0))
# #单元格批量读取,读取第一列所有内容
# print("col num:",sht1.col_values(0))

# #导入xlwt模块
# from xlwt import Workbook
# #创建工作簿
# wb = Workbook()
# #添加sheet页
# Sheet1 = wb.add_sheet('Sheet1')
# #写入行，列，值
# Sheet1.write(0,0,'Hello')
# #保存工作簿
# wb.save('hello.xls')

#导入包
from xlrd import open_workbook
from xlutils.copy import copy
#打开需要编辑的表格
rb = open_workbook("data.et")
#通过xlutils包复制当前表格
wb = copy(rb)
#获取需要编辑的sheet页
s = wb.get_sheet(0)
#写入行，列，值
s.write(0,0,'hello')
#保存编辑后的表格，可以新命名，可以覆盖原文件
wb.save('data.et')