# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 10:42:08 2020

@author: 016322684
"""

#导入tkinter包
import tkinter as tk
#创建窗口
window=tk.Tk()
#窗口命名
window.title('listbox演示')
#设置窗口大小
window.geometry('300x200')
#创建变量
var = tk.StringVar()
#创建标签
l = tk.Label(window, bg='green', fg='yellow',font=('Arial', 12), width=10, textvariable=var)
#放置标签
l.pack()
#定义打印函数
def printList(event):
#将变量var设置为当前列表选中项
    var.set(lb.get(lb.curselection()))
#创建一个listbox
lb = tk.Listbox(window)
#给listbox添加选项
for item in ['选项1','选项2','选项3']:
    lb.insert('end',item)
#绑定事件
lb.bind('<Double-Button-1>',printList)
#放置listbox
lb.pack()
#主窗口循环显示
window.mainloop()