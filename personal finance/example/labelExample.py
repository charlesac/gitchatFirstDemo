# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 10:42:08 2020

@author: 016322684
"""

#导入tkinter包
import tkinter as tk
import tkinter.font as tkFont
#创建窗口
window=tk.Tk()
#窗口命名
window.title('label演示')
window.geometry('300x200')
#创建1个标签组件
label1=tk.Label(window,text='这是一个标签',bg='blue',fg='red',font=tkFont.Font(family="微软雅黑",size=12,weight='bold'))
label1.pack()
window.mainloop()