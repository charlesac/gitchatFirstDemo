# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 10:42:08 2020

@author: 016322684
"""

#导入tkinter包
import tkinter as tk
import tkinter.font as tkFont
#创建窗口
window=tk.Tk()
#窗口命名
window.title('grid演示')
window.geometry('300x200')
#创建6个标签组件
tk.Label(window,text='组件1',bg='blue',font=tkFont.Font(family="微软雅黑",size=12,weight='bold')).grid(row=0,column=0,columnspan=3,sticky=tk.E+tk.W)
tk.Label(window,text='组件2',bg='red',font=tkFont.Font(family="微软雅黑",size=12,weight='bold')).grid(row=1,column=0)
tk.Label(window,text='组件3',bg='yellow',font=tkFont.Font(family="微软雅黑",size=12,weight='bold')).grid(row=2,column=0)
tk.Label(window,text='组件4',bg='green',font=tkFont.Font(family="微软雅黑",size=12,weight='bold')).grid(row=1,column=1,rowspan=2,sticky=tk.N+tk.S)
tk.Label(window,text='组件5',bg='red',font=tkFont.Font(family="微软雅黑",size=12,weight='bold')).grid(row=1,column=2)
tk.Label(window,text='组件6',bg='yellow',font=tkFont.Font(family="微软雅黑",size=12,weight='bold')).grid(row=2,column=2)

window.mainloop()