# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 21:53:55 2020

@author: charl
"""

#导入tkinter包
import tkinter as tk
#创建窗口
window=tk.Tk()
#窗口命名
window.title('按键演示')
#设置窗口大小
window.geometry('300x200')
#定义按键动作事件
def helloworld():
    print('hello world!')
#创建按键
button=tk.Button(window,text='hello',command=helloworld)
#将按键布置在主窗口
button.pack()
#主窗口循环显示
window.mainloop()