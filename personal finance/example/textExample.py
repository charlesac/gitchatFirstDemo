# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 10:42:08 2020

@author: 016322684
"""

# #导入tkinter包
# import tkinter as tk
# #创建窗口
# window=tk.Tk()
# #窗口命名
# window.title('text演示')
# #text组件
# text = tk.Text(window)
# #放置text组件，放置在左侧，完全填充
# text.pack(side='left',fill='both')
# #创建滚动条组件
# scroll = tk.Scrollbar(window)
# #放置scroll组件，布置在右侧，延竖直方向填充
# scroll.pack(side='right',fill=tk.Y)
# #设置滚动条的动作
# scroll.config(command=text.yview)
# #绑定text的滚动到scroll
# text.config(yscrollcommand=scroll.set)
# #主窗口循环显示
# window.mainloop()


#导入tkinter包
import tkinter as tk
#创建窗口
window=tk.Tk()
#窗口命名
window.title('text演示')
#设置窗口大小
window.geometry('300x200')
#创建text组件,设置高度与宽度
text = tk.Text(window,width=30,height=10)
#放置text组件
text.pack()
#在第一行第一例添加文本
text.insert('0.0','第一行第一列')
#定义打印函数
def printText():
    print(text.get('0.0','end'))
#添加按钮，点击打印text的内容
button=tk.Button(window,text='打印',command=printText)
#放置按钮
button.pack()
#主窗口循环显示
window.mainloop()