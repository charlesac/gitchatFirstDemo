# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 10:42:08 2020

@author: 016322684
"""

#导入tkinter包
import tkinter as tk
#创建窗口
window=tk.Tk()
#窗口命名
window.title('pack演示')
#设置窗口大小
window.geometry('300x200')
#创建四个标签
labelt = tk.Label(window,text='top',bg='yellow')
labelb = tk.Label(window,text='bottom',bg='green')
labell = tk.Label(window,text='left',bg='red')
labelr = tk.Label(window,text='right',bg='blue')
#分别布局在四个位置
labelt.pack(side='top')
labelb.pack(side='bottom')
labell.pack(side='left')
labelr.pack(side='right')
#主窗口循环显示
window.mainloop()