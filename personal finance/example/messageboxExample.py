#导入tkinter包
import tkinter as tk
import tkinter.messagebox
#创建窗口
window=tk.Tk()
#窗口命名
window.title('massagebox口演示')
#设置窗口大小
window.geometry('300x200')
#创建messagebox组件，分别为提示、警告、错误图标
tk.messagebox.showinfo('提示','你确定要删除吗？')
tk.messagebox.showwarning(title='提示', message='你确定要删除吗？')
tk.messagebox.showerror('提示','你确定要删除吗？')
#主窗口循环显示
window.mainloop()