# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 22:03:18 2020

@author: charl
"""

#导入tkinter包
import tkinter as tk
#创建窗口
window=tk.Tk()
#窗口命名
window.title('主窗口演示')
#设置窗口大小
window.geometry('300x200')

#主窗口循环显示
window.mainloop()